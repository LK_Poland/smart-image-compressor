#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <image.h>

#include <QMainWindow>

#include <QClipboard>
#include <QFileInfo>
#include <QGuiApplication>
#include <QMenu>
#include <QMouseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

        bool openImage(const QString& path);

    private slots:
        void copyToClipboard();
        void restoreSize();

    protected:
        void paintEvent(QPaintEvent* event) override;
        void mousePressEvent(QMouseEvent *event) override;

    private:
        Ui::MainWindow *ui;
        QImage image;
        QMenu popup;
};
#endif // MAINWINDOW_H
