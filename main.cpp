#include "mainwindow.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QFileInfo>

#include <iostream>

static const QString& AppName = "Smart Image Compressor";
static const QString& AppVersion = "1.0";
static const QString& AppAuthor = "LK";
static const QString& AppWebSite = "http://my3soft.getenjoyment.net";

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setApplicationName(AppName);
    QCoreApplication::setApplicationVersion(AppVersion);

    QCommandLineOption optLevel(QStringList() << "l" << "level",
                                "Bigger number results in smaller file, smaller number results in better image quality.",
                                "level", "65");

    QCommandLineParser parser;
    parser.setApplicationDescription("\n" + AppName + "\n[!!] Experimental lossy image compression algorithm.\n"
                                     + QString("ver. %1 (Qt %2) GPLv3, written by %3, site: %4").arg(AppVersion).arg(qVersion()).arg(AppAuthor).arg(AppWebSite));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(optLevel);
    parser.addPositionalArgument("file", "File to (de)compress.");
    parser.process(a);

    const QStringList& args = parser.positionalArguments();
    if (args.empty())
    {
        parser.showHelp();
    }
    const QFileInfo info(args[0]);
    if (!info.exists())
    {
        std::cout << "File \"" << info.filePath().toStdString() << "\" doesn't exist." << std::endl;
        return 1;
    }
    if (info.suffix().toLower() == "sic")
    {
        MainWindow w;
        if (w.openImage(info.filePath()))
        {
            w.show();
            return a.exec();
        }
        else
        {
            std::cout << "File \"" << info.filePath().toStdString() << "\" is invalid." << std::endl;
            return 2;
        }
    }
    else
    {
        bool ok;
        const QString& strLevel = parser.value(optLevel);
        const uint32_t level = strLevel.toUInt(&ok);
        if (ok)
        {
            std::cout << "Level set to " << level << "." << std::endl;
        }
        QFileInfo infoOut(QString("%1\\%2.sic").arg(info.absolutePath()).arg(info.baseName()));
        if (infoOut.exists())
        {
            std::cout << "File \"" << infoOut.filePath().toStdString() << "\" already exists." << std::endl;
            return 3;
        }
        std::cout << "Compressing \"" << info.fileName().toStdString()
                  << "\" to \"" << infoOut.fileName().toStdString() << "\"." << std::endl;
        ImagePtr image = Image::compress(info.filePath(), level);
        image->save(infoOut.filePath());
        std::cout << image->partition.size() << " partitions saved." << std::endl;
    }

    return 0;
}
