#include "image.h"

void Image::reset()
{
    width = 0;
    height = 0;
    valid = false;
    partition.clear();
}

Image::Image()
{
    reset();
}

bool Image::isValid() const
{
    return valid;
}

QImage Image::toQImage() const
{
    QImage img(width, height, QImage::Format_RGB32);
    if (!valid)
    {
        return img;
    }
    Q_FOREACH (const PartitionPtr& pp, partition)
    {
        Q_FOREACH (const uint32_t& idx, pp->index)
        {
            const int j = idx * 2 % width;
            const int i = idx * 2 / width * 2;
            img.setPixelColor(j, i, pp->pixel[0]);
            img.setPixelColor(j, i + 1, pp->pixel[1]);
            img.setPixelColor(j + 1, i, pp->pixel[2]);
            img.setPixelColor(j + 1, i + 1, pp->pixel[3]);
        }
    }
    return img;
}

bool Image::save(const QString &path) const
{
    QFile fout(path);
    if (!fout.open(QIODevice::WriteOnly) ||
        !valid)
    {
        return false;
    }
    QDataStream dout(&fout);
    dout.setByteOrder(QDataStream::BigEndian);
    dout.writeRawData("SIC", 3);
    dout << (uint8_t)1;
    QBuffer buffer;
    buffer.open(QBuffer::WriteOnly);
    QDataStream dbuf(&buffer);
    dbuf.writeRawData((const char*)&width, 3);
    dbuf.writeRawData((const char*)&height, 3);
    Q_FOREACH (const PartitionPtr& pp, partition)
    {
        for (int k = 0 ; k < 4 ; ++k)
        {
            const QRgb& rgb = pp->pixel[k].rgb();
            dbuf.writeRawData((const char*)&rgb, 3);
        }
        const uint32_t& size = pp->index.size();
        dbuf.writeRawData((const char*)&size, 3);
        Q_FOREACH (const uint32_t& idx, pp->index)
        {
            dbuf.writeRawData((const char*)&idx, 3);
        }
    }
    dout << qCompress(buffer.data(), 9);
    return true;
}

ImagePtr Image::open(const QString &path)
{
    ImagePtr img = std::make_shared<Image>();
    QFile fin(path);
    if (!fin.open(QIODevice::ReadOnly))
    {
        return img;
    }
    QDataStream din(&fin);
    din.setByteOrder(QDataStream::BigEndian);
    char sig[4] = {0};
    din.readRawData(sig, 3);
    if (qstrcmp(sig, "SIC") != 0)
    {
        return img;
    }
    din.skipRawData(1);
    QByteArray packed;
    din >> packed;
    const QByteArray& raw = qUncompress(packed);
    QBuffer buffer;
    buffer.setBuffer((QByteArray*)&raw);
    buffer.open(QBuffer::ReadOnly);
    QDataStream dbuf(&buffer);
    dbuf.readRawData((char*)&img->width, 3);
    dbuf.readRawData((char*)&img->height, 3);
    while (!buffer.atEnd())
    {
        QRgb rgb;
        PartitionPtr pp = std::make_shared<Partition>();
        img->partition.append(pp);
        for (int k = 0 ; k < 4 ; ++k)
        {
            dbuf.readRawData((char*)&rgb, 3);
            pp->pixel[k] = rgb;
        }
        uint32_t size = 0;
        dbuf.readRawData((char*)&size, 3);
        for (uint32_t k = 0 ; k < size ; ++k)
        {
            uint32_t idx = 0;
            dbuf.readRawData((char*)&idx, 3);
            pp->index.append(idx);
        }
    }
    img->valid = true;
    return img;
}

ImagePtr Image::compress(const QImage& img, const double similarity)
{
    const int w = img.width() - img.width() % 2;
    const int h = img.height() - img.height() % 2;

    ImagePtr out = std::make_shared<Image>();
    out->width = w;
    out->height = h;

    if (img.isNull())
    {
        return out;
    }

    uint32_t ctr = 0;
    for (int i = 0 ; i + 1 < h ; i += 2)
    {
        for (int j = 0 ; j + 1 < w ; j += 2, ++ctr)
        {
            PartitionPtr pp = std::make_shared<Partition>();
            pp->pixel[0] = img.pixelColor(j, i);
            pp->pixel[1] = img.pixelColor(j, i + 1);
            pp->pixel[2] = img.pixelColor(j + 1, i);
            pp->pixel[3] = img.pixelColor(j + 1, i + 1);
            for (int k = 0 ; k < 4 ; ++k)
            {
                int r, g, b;
                pp->pixel[k].getRgb(&r, &g, &b);
                pp->r += r;
                pp->g += g;
                pp->b += b;
            }
            pp->index.append(ctr);
            out->partition.append(pp);
        }
    }

    const double optStep = similarity / 3;

    out->partMerge(similarity);

    for (int i = similarity + optStep ; i <= 3 * similarity ; i += optStep)
    {
        out->partOptimize(i);
    }

    /*
    int onlyOneIdx = 0;
    for (int j = 0 ; j < out->partition.size() ; ++j)
    {
        PartitionPtr pp = out->partition.at(j);
        if (pp->index.size() == 1)
        {
            ++onlyOneIdx;
        }
    }
    */

    out->valid = true;

    return out;
}

ImagePtr Image::compress(const QString& path, const double similarity)
{
    return compress(QImage(path), similarity);
}

void Image::partMerge(const double similarity)
{
    for (int j = 0 ; j < partition.size() ; ++j)
    {
        PartitionPtr pp = partition.at(j);
        for (int i = j + 1 ; i < partition.size() ;)
        {
            PartitionPtr ppNext = partition.at(i);
            if (pp->compareTo(ppNext) <= similarity)
            {
                pp->index.append(ppNext->index);
                partition.removeAt(i);
            }
            else
            {
                ++i;
            }
        }
    }
}

void Image::partOptimize(const double similarity)
{
    for (int j = 0 ; j < partition.size() ; ++j)
    {
        PartitionPtr pp = partition.at(j);
        if (pp->index.size() != 1)
        {
            continue;
        }
        for (int i = j + 1 ; i < partition.size() ;)
        {
            PartitionPtr ppNext = partition.at(i);
            if (//ppNext->index.size() <= pp->index.size() &&
                pp->compareTo(ppNext) <= similarity)
            {
                pp->index.append(ppNext->index);
                partition.removeAt(i);
            }
            else
            {
                ++i;
            }
        }
    }
}
