#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::openImage(const QString& path)
{
    ImagePtr img = Image::open(path);
    image = img->toQImage();
    restoreSize();
    const QFileInfo info(path);
    setWindowTitle(QString("%1 size:%2KB partitions:%3")
                   .arg(info.fileName()).arg(info.size() / 1024).arg(img->partition.size()));
    setWindowIcon(QPixmap::fromImage(image));
    popup.clear();
    //popup.addSection(info.fileName());
    popup.addAction("Copy to clipboard", this, SLOT(copyToClipboard()));
    popup.addAction("Restore size", this, SLOT(restoreSize()));
    popup.addSeparator();
    popup.addAction("Close", this, SLOT(close()));
    return img->isValid();
}

void MainWindow::copyToClipboard()
{
    QGuiApplication::clipboard()->setImage(image);
}

void MainWindow::restoreSize()
{
    resize(image.size());
}

void MainWindow::paintEvent(QPaintEvent*)
{
    if (image.isNull())
    {
        return;
    }
    QPainter p(this);
    p.drawImage(rect(), image, image.rect());
}

void MainWindow::mousePressEvent(QMouseEvent* e)
{
    if (e->button() == Qt::RightButton)
    {
        popup.exec(e->globalPos());
    }
}
