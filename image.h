#ifndef IMAGE_H
#define IMAGE_H

#include <memory.h>

#include <QtMath>

#include <QBuffer>
#include <QDataStream>
#include <QDebug>
#include <QFile>
#include <QImage>
#include <QList>
#include <QPainter>

struct Partition;

using PartitionPtr = std::shared_ptr<Partition>;

struct Partition
{
        QColor pixel[4];
        QList<uint32_t> index;
        uint32_t r = 0, g = 0, b = 0;

        virtual double compareTo(const PartitionPtr& pp)
        {
            return qSqrt(qPow(pp->r - r, 2) + qPow(pp->g - g, 2) + qPow(pp->b - b, 2));
            //return qSqrt(0.3 * qPow(pp->r - r, 2) + 0.59 * qPow(pp->g - g, 2) + 0.11 * qPow(pp->b - b, 2));
        }
};

class Image;

using ImagePtr = std::shared_ptr<Image>;

class Image
{
    private:
        uint32_t width = 0;
        uint32_t height = 0;
        bool valid = false;

    public:
        QList<PartitionPtr> partition;

        Image();

        bool isValid() const;
        QImage toQImage() const;

        bool save(const QString& path) const;
        static ImagePtr open(const QString& path);

        static ImagePtr compress(const QImage& img, const double similarity = 65);
        static ImagePtr compress(const QString& path, const double similarity = 65);

    private:
        void reset();
        void partMerge(const double similarity);
        void partOptimize(const double similarity);
};

#endif // IMAGE_H
